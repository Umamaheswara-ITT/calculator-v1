import { start, get } from 'prompt';
import { ScientificCalculator, SimpleCalculator } from './calculator';

start();

console.log('Allowed Operations - add/sub/mul/div/pow');
get(['operation', 'operand1', 'operand2'], function (err, result) {
  if (err) { return onErr(err); }
  const operand1 = parseInt(result.operand1 as string);
  const operand2 = parseInt(result.operand2 as string);
  const operation = result.operation as string;
  const cal = new ScientificCalculator()
  if (operation === 'add') {
    console.log('Output:', cal.addition(operand1, operand2));
  } else if (operation === 'sub') {
    console.log('Output:', cal.subtraction(operand1, operand2));
  } else if (operation === 'mul') {
    console.log('Output:', cal.multiplication(operand1, operand2));
  } else if (operation === 'div') {
    console.log('Output:', cal.division(operand1, operand2));
  } else if (operation === 'pow') {
    console.log('Output:', cal.pow(operand1, operand2));
  } else {
    console.error('Unknown Operation!')
  }
});

function onErr(err: object) {
  console.log(err);
  return 1;
}