interface Calculator {
  addition(input1: number, input2: number): number;
  subtraction(input1: number, input2: number): number;
  multiplication(input1: number, input2: number): number;
  division(input1: number, input2: number): number;
}

interface AdvanceCalculator extends Calculator {
  pow(input1: number, input2: number): number;
}

export class SimpleCalculator implements Calculator {
  public addition(input1: number, input2: number): number {
    return input1 + input2;
  }
  public subtraction(input1: number, input2: number): number {
    return input1 - input2;
  }
  public multiplication(input1: number, input2: number): number {
    return input1 * input2;
  }
  public division(input1: number, input2: number): number {
    return input1 / input2
  }
}

export class ScientificCalculator extends SimpleCalculator implements AdvanceCalculator {
  Operation: string = "";
  pow(input1: number, input2: number): number {
    return Math.pow(input1, input2);
  }
}